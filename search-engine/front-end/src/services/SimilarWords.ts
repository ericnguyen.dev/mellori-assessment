import { apiRouteEnum } from "../enums/ApiRoutes";
import { axiosInstance } from "./axios/index";

export async function getSimilarWords(word: string) {
  return axiosInstance.get(`${apiRouteEnum.GetSimilarWord}/${word}`);
}

export async function removeMostSimilarWord(word: string) {
  return axiosInstance.delete(`${apiRouteEnum.DeleteMostSimilarWord}/${word}`);
}
