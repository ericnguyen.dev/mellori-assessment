import { apiRouteEnum } from "../enums/ApiRoutes";
import { axiosInstance } from "./axios/index";

export async function addWord(newWord: string) {
  return axiosInstance.post(apiRouteEnum.AddWord, {
    newWord,
  });
}
