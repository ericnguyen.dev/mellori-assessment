import { baseApiUrl } from "./../../config";
import axios from "axios";

export const axiosInstance = axios.create({
  baseURL: baseApiUrl,
  headers: {
    "Content-Type": "application/json",
    timeout: 60000,
  },
});
