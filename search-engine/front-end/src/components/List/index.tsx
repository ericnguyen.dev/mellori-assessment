import "./styles.css";

type ListProps = {
  items: string[];
};

function List({ items }: ListProps) {
  return (
    <ul className="list">
      {items.map((item) => (
        <li className="list-item" key={item}>
          {item}
        </li>
      ))}
    </ul>
  );
}

export default List;
