import "./styles.css";

export enum NotificationType {
  Alert = "alert",
  Success = "success",
  Warning = "warning",
  Error = "error",
}

type NotificationProps = {
  content: string;
  type: NotificationType;
};

function Notification({
  content,
  type = NotificationType.Alert,
}: NotificationProps) {
  return (
    <div className={`notification ${type}`}>
      <p className="notification-content">{content.trim()}</p>
    </div>
  );
}

export default Notification;
