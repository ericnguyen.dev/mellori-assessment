import React from "react";
import "./styles.css";

interface DropdownProps {
  value: string;
  options: string[];
  onChange: (newValue: string) => void;
}

const Dropdown: React.FC<DropdownProps> = ({ value, options, onChange }) => {
  const handleSelectChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    onChange(event.target.value);
  };

  return (
    <select className="dropdown" value={value} onChange={handleSelectChange}>
      {options.map((option) => (
        <option className="dropdown-item" key={option} value={option}>
          {option}
        </option>
      ))}
    </select>
  );
};

export default Dropdown;
