export enum apiRouteEnum {
  GetSimilarWord = "/similar-words",
  AddWord = "/words",
  DeleteMostSimilarWord = "/similar-words",
}
