import { useEffect, useState } from "react";
import "./App.css";
import TextInput from "./components/TextInput";
import Dropdown from "./components/Dropdown";
import Button from "./components/Button";
import * as SimilarWordsService from "../src/services/SimilarWords";
import * as WordsService from "../src/services/Words";

import { AxiosResponse } from "axios";
import List from "./components/List";
import Notification, { NotificationType } from "./components/Notification";

const searchEngineActions = {
  ThreeMostSimilarWord: "Show 3 most similar words",
  AddNewWord: "Add new word",
  RemoveTheMostSimilarWord: "Remove the most similar word",
};

Object.freeze(searchEngineActions);

function App() {
  const [keyword, setKeyword] = useState("");
  const [action, setAction] = useState(
    searchEngineActions.ThreeMostSimilarWord
  );
  const [wordList, setWordList] = useState([]);
  const [notification, setNotification] = useState<any>(null);

  useEffect(() => {
    const timerId = setTimeout(() => {
      setNotification(null);
    }, 3000);
    return () => {
      clearInterval(timerId);
    };
  }, [notification]);

  const handleKeywordChange = (value: string) => setKeyword(value);
  const handleActionChange = (value: string) => setAction(value);

  const getSimilarWords = () => {
    SimilarWordsService.getSimilarWords(keyword)
      .then((res: AxiosResponse) => {
        setWordList(res.data);
      })
      .catch(() => {
        setNotification({
          type: NotificationType.Error,
          message: "Failed to fetch similar words, please try again",
        });
      });
  };

  const addNewWord = () => {
    WordsService.addWord(keyword)
      .then((res: AxiosResponse) => {
        setNotification({
          type: NotificationType.Success,
          message: "Successfully added new word",
        });
      })
      .catch(() => {
        setNotification({
          type: NotificationType.Error,
          message: "Failed to add new words, please try again",
        });
      });
  };

  const removeMostSimilarWord = () => {
    SimilarWordsService.removeMostSimilarWord(keyword)
      .then((res: AxiosResponse) => {
        setNotification({
          type: NotificationType.Success,
          message: `Successfully removed [${res.data}]`,
        });
      })
      .catch(() => {
        setNotification({
          type: NotificationType.Error,
          message: "Remove failed, please try again",
        });
      });
  };

  const handleExecuteAction = () => {
    setWordList([]);
    switch (action) {
      case searchEngineActions.ThreeMostSimilarWord:
        getSimilarWords();
        break;
      case searchEngineActions.AddNewWord:
        addNewWord();
        break;
      case searchEngineActions.RemoveTheMostSimilarWord:
        removeMostSimilarWord();
        break;
    }
  };

  return (
    <div className="app">
      <div className="container">
        <div className="search-controls">
          <TextInput
            value={keyword}
            onChange={handleKeywordChange}
            placeholder="Enter keyword here..."
          />
          <Dropdown
            value={action}
            options={Object.values(searchEngineActions)}
            onChange={handleActionChange}
          />
          <Button onClick={handleExecuteAction} text="⚡Execute" />
        </div>
        <div className="result">
          {notification && (
            <Notification
              content={notification.message}
              type={NotificationType.Warning}
            />
          )}
          {wordList && <List items={wordList} />}
        </div>
      </div>
    </div>
  );
}

export default App;
