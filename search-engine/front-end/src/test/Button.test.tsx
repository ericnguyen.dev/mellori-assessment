import { render, fireEvent, screen } from "@testing-library/react";
import Button from "../components/Button";

describe("Button", () => {
  test("renders with correct text", () => {
    const buttonText = "Click me";
    render(<Button onClick={() => {}} text={buttonText} />);

    const buttonElement = screen.getByText(buttonText);
    expect(buttonElement).toBeInTheDocument();
  });

  test("calls onClick when clicked", () => {
    const onClickMock = jest.fn();
    render(<Button onClick={onClickMock} text="Click me" />);

    const buttonElement = screen.getByText("Click me");
    fireEvent.click(buttonElement);

    expect(onClickMock).toHaveBeenCalledTimes(1);
  });

  test("is disabled when disabled prop is true", () => {
    render(<Button onClick={() => {}} text="Click me" disabled={true} />);

    const buttonElement = screen.getByText("Click me");
    expect(buttonElement).toBeDisabled();
  });
});
