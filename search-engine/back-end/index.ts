import express, { Application } from "express";
import cors from "cors";

import similarWordRoutes from "./src/routes/similarWord";
import wordRoutes from "./src/routes/word";
import { PORT } from "./config";
import * as fileContentStore from "./src/corpus";

const app: Application = express();

app.use(
  cors({
    origin: "http://localhost:3000", // TODO: set with env
  })
);

app.use(express.json());

fileContentStore.readContent().catch((err) => {
  throw err;
});

app.get("/ping", async (_req, res) => {
  res.send({
    message: "Server is running...",
  });
});

app.use("/similar-words", similarWordRoutes);
app.use("/words", wordRoutes);

app.listen(PORT, () => {
  console.log("Server is running on port", PORT);
});
