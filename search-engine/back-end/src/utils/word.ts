import { distance } from "fastest-levenshtein";

interface WordDistance {
  word: string;
  distance: number;
}

function findSimilar(query: string, words: Set<string>, limit = 1): string[] {
  if (words.size === 0) return [];
  const distances: WordDistance[] = Array.from(words).map((w) => ({
    word: w,
    distance: distance(query.toLowerCase(), w),
  }));
  const sorted: WordDistance[] = distances.sort((a, b) => a.distance - b.distance);

  return sorted.slice(0, limit).map((d) => d.word);
}

function textToWordSet(text: string, toLowercase = false): Set<string> {
  const cleanedText: string = text
    .trim()
    .replace(/[^\w\s]|_/g, " ")
    .replace(/\s+/g, " ");
  const words: string[] = cleanedText.split(" ");
  const wordSet = new Set(words);

  return toLowercase ? new Set(Array.from(wordSet).map((word) => word.toLowerCase())) : wordSet;
}

export { findSimilar, textToWordSet };
