export enum ValidateType {
  Body = "body",
  Params = "params",
  Query = "query",
}
