import { Router } from "express";

import validateRequest from "../middleware/validateRequest";
import { ValidateType } from "../enums/ValidateType";
import { addNewWordParamsSchema } from "../validation/params/addNewWordParamsSchema";
import { addNewWord } from "../controllers/word";

const router = Router();

router.post("/", validateRequest(addNewWordParamsSchema, ValidateType.Body), addNewWord);

export default router;
