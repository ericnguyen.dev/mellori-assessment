import { Router } from "express";

import { deleteSimilarWord, getSimilarWords } from "../controllers/similarWord";
import validateRequest from "../middleware/validateRequest";
import { ValidateType } from "../enums/ValidateType";
import { deleteSimilarWordsParamsSchema } from "../validation/params/deleteSimilarWordsParamsSchema";
import { getSimilarWordsParamsSchema } from "../validation/params/getSimilarWordsParamsSchema";
import { getSimilarWordsQuerySchema } from "../validation/query/getSimilarWordsQuerySchema";

const router = Router();

router.get(
  "/:word",
  validateRequest(getSimilarWordsParamsSchema, ValidateType.Params),
  validateRequest(getSimilarWordsQuerySchema, ValidateType.Query),
  getSimilarWords
);

router.delete("/:word", validateRequest(deleteSimilarWordsParamsSchema, ValidateType.Params), deleteSimilarWord);

export default router;
