import fs from "fs";
import path from "path";

let content: string;

async function readContent(): Promise<void> {
  try {
    const file = await fs.promises.readFile(path.resolve(__dirname, "./assets/hemingway.txt"));
    content = file.toString();
  } catch (err) {
    throw new Error(`Error reading file`);
  }
}

async function addWord(word: string): Promise<void> {
  if (!word) return;
  content += ` ${word}`;
}

async function removeWord(word: string): Promise<void> {
  if (!word || !content) return;
  content = content.replace(new RegExp(word, "gi"), "");
}

async function removeWordBulk(words: string[]): Promise<void> {
  if (!words.length || !content) return;
  for (const word of words) {
    await removeWord(word);
  }
}

export { content, readContent, addWord, removeWord, removeWordBulk };
