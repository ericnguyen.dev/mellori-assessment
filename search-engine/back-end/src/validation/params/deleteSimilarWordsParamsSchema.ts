import Joi from "joi";

export const deleteSimilarWordsParamsSchema = Joi.object().keys({
  word: Joi.string().min(1).required(),
});
