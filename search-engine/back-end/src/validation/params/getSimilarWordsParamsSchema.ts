import Joi from "joi";

export const getSimilarWordsParamsSchema = Joi.object().keys({
  word: Joi.string().min(1).required(),
});
