import Joi from "joi";

export const addNewWordParamsSchema = Joi.object().keys({
  newWord: Joi.string().min(1).required(),
});
