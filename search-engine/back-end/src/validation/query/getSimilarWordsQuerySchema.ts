import Joi from "joi";

export const getSimilarWordsQuerySchema = Joi.object().keys({
  limit: Joi.number().min(1).optional(),
});
