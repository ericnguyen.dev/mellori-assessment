import { Request, Response, NextFunction } from "express";
import { ObjectSchema } from "joi";

function validateRequest(schema: ObjectSchema, property: "body" | "params" | "query") {
  return function (req: Request, res: Response, next: NextFunction) {
    const result = schema.validate(req[property]);
    if (result.error) {
      return res.status(400).json({ error: result.error.details[0].message });
    }
    next();
  };
}

export default validateRequest;
