import { textToWordSet } from "../utils/word";

describe("textToWordSet", () => {
  test("should correctly split text into word set without converting to lowercase", () => {
    const text = "This is a sample text";
    const wordSet = textToWordSet(text);
    expect(wordSet.size).toBe(5);
    expect(wordSet.has("This")).toBe(true);
    expect(wordSet.has("is")).toBe(true);
    expect(wordSet.has("a")).toBe(true);
    expect(wordSet.has("sample")).toBe(true);
    expect(wordSet.has("text")).toBe(true);
    expect(wordSet.has("Text")).toBe(false);
  });

  test("should correctly split text into word set and convert to lowercase", () => {
    const text = "This is a sample text";
    const wordSet = textToWordSet(text, true);
    expect(wordSet.size).toBe(5);
    expect(wordSet.has("this")).toBe(true);
    expect(wordSet.has("is")).toBe(true);
    expect(wordSet.has("a")).toBe(true);
    expect(wordSet.has("sample")).toBe(true);
    expect(wordSet.has("text")).toBe(true);
    expect(wordSet.has("Text")).toBe(false);
  });
});
