import { Request, Response } from "express";

import { findSimilar, textToWordSet } from "../utils/word";
import * as corpus from "../corpus";
import { HttpStatusCode } from "../enums/HttpStatusCode";

export const readCorpus = async (req: Request, res: Response) => {
  try {
    const content = corpus.content;
    res.send(content);
  } catch (error) {
    res.status(HttpStatusCode.InternalServerError).send();
  }
};

export const getSimilarWords = async (req: Request, res: Response) => {
  try {
    const word: string = req.params.word;
    const limit: number = Number(req.query.limit) || 3;
    const wordSet: Set<string> = textToWordSet(corpus.content, true);
    const similarWords: string[] = findSimilar(word, wordSet, limit);
    res.send(similarWords);
  } catch (error) {
    res.status(HttpStatusCode.InternalServerError).send();
  }
};

export const deleteSimilarWord = async (req: Request, res: Response) => {
  try {
    const word: string = req.params.word;

    const wordSet = textToWordSet(corpus.content, true);
    const similarWords: string[] = findSimilar(word, wordSet, 1);

    await corpus.removeWordBulk(similarWords);

    res.status(HttpStatusCode.Ok).send(similarWords);
  } catch (error) {
    res.status(HttpStatusCode.InternalServerError).send();
  }
};
