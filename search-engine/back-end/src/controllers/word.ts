import { Request, Response } from "express";

import * as corpus from "../corpus";
import { HttpStatusCode } from "../enums/HttpStatusCode";

export const addNewWord = async (req: Request, res: Response) => {
  try {
    const newWord: string = req.body.newWord;
    await corpus.addWord(newWord);
    res.status(HttpStatusCode.Created).send();
  } catch (error) {
    res.status(HttpStatusCode.InternalServerError).send();
  }
};
