# Mellori-assessment 

## Requirement and assets
    https://gitlab.com/kvytech/assessment-test

## Project Scope
- This project is not intended to cover all technologies; it focuses on providing a simple solution to the challenge at hand.
- Unnecessary technologies such as Redux, database implementation, form validation... will not be included, as they do not make sense for the standalone task.
- Unit tests will be performed, but the goal is not to achieve high test coverage.
- React components will be built specifically for this project and may not be designed for reuse, considering the time limit for completing the challenge.
- Due to the time limit, some side edges and detailed implementations may be ignored.

## Getting Started
To start the project, you will need to run both the front-end and back-end servers using the following commands:
```
   $ npm install
   $ npm start
```

## Demo
![Alt Text](./quick-demo.gif)

## Contact
If you have any questions or need further information, please feel free to contact me at ericnguyen.dev@gmail.com
## Thanks
Thanks a lot for this exciting challenge, happy codding :)

